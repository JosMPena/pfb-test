class CoinUpdate < ApplicationRecord
  belongs_to :user, dependent: :destroy
  validates_presence_of :action, :amount, :reason
end

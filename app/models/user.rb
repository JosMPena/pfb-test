class User < ApplicationRecord
  has_many :coin_updates
  validates :username, uniqueness: true
  validates :email, length: {in: 8..100},
            format: {with: /\A([a-z0-9\-_\.]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}, on: :create
  validates :level, presence: true, numericality: {greater_than: -1}
  validates :coins, presence: true, numericality: {greater_than: -1}

  %w[increase_coins decrease_coins reset_coins].each do |method|
    define_method method do |attrs|
      attrs[:amount] ||= 0
      amount = method == 'increase_coins' ? coins + attrs[:amount] : coins - attrs[:amount]
      amount = 0 if method == 'reset_coins'
      action = method.split('_').first

      transaction do
        update(coins: amount)
        coin_updates.create(action: action, amount: amount, reason: attrs[:reason])
      end
    end
  end

end

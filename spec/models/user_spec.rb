require 'rails_helper'

describe User do
  subject { FactoryGirl.build :user }
  before { subject.coins = 400 }

  it { is_expected.to be_valid }

  describe :attributes do
    it { is_expected.to respond_to :username }
    it { is_expected.to respond_to :email }
    it { is_expected.to respond_to :coins }
  end

  it 'should #increase_coins' do
    amount = 1000
    expect { subject.increase_coins(amount: amount, reason: 'Prize') }
      .to change { subject.coins }.by(amount)
  end

  it 'should #decrease_coins' do
    amount = 200
    expect { subject.decrease_coins(amount: amount, reason: 'Penalty') }
      .to change { subject.coins }.by(-amount)
  end

  it 'should #reset_coins' do
    expect { subject.reset_coins(reason: 'Reset') }
      .to change { subject.coins }.from(400).to(0)
  end

  describe 'Coin changes' do
    it 'should log the coin changes' do
      %w[increase_coins decrease_coins reset_coins].each do |action|
        expect { subject.send(action, amount: 50, reason: 'Gift') }
          .to change { CoinUpdate.count }.by(1)
      end
    end

    it 'should not change coins without a reason' do
      %w[increase_coins decrease_coins reset_coins].each do |action|
        expect { subject.send(action, amount: 50) }
          .to_not(change { CoinUpdate.count })
      end
    end
  end
end

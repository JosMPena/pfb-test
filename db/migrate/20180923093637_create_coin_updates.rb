class CreateCoinUpdates < ActiveRecord::Migration[5.0]
  def change
    create_table :coin_updates do |t|
      t.references :user
      t.string :action
      t.integer :amount
      t.string :reason

      t.timestamps
    end
  end
end
